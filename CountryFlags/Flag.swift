import UIKit

public class Flag {

    public static func image(for code: String) -> UIImage? {
        return UIImage(named: code.uppercased(), in: Bundle._module, compatibleWith: nil)
    }
}
